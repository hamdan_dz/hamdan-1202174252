<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.5">
    <title>WAD Store</title>

    <!-- <link rel="canonical" href="https://getbootstrap.com/docs/4.3/examples/pricing/"> -->
<!-- Bootstrap core CSS -->
<!-- <link href="/Project WADStore/dist-css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"> -->

<!-- bootstrap -->
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<!-- js -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

<?php
include_once("koneksi.php");
session_start();
$result = mysqli_query($mysqli, "SELECT * FROM users ORDER BY id DESC");
?>
    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
    <!-- styles -->
    <link href="pricing.css" rel="stylesheet">
  </head>
  <body>


    <div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom shadow-sm">
  <h5 class="my-0 mr-md-auto font-weight-normal"><img src="img/EAD.png" style="width:160px;height:40px;"></h5>
  <nav class="my-2 my-md-0 mr-md-3">
    
    
    
    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Hello, <?= $_SESSION['username']; ?></a>
    <div class="dropdown-menu">
      <a class="dropdown-item" href="indexfull.php">Home</a>
      <a class="dropdown-item" href="profile.php">Profile</a>
      <a class="dropdown-item" href="cart.php">Cart</a>
      <div class="dropdown-divider"></div>
      <a class="dropdown-item" href="logout.php">Logout</a>
    </div>
  </li>
  </nav>
</div>

<div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
  <h1 class="display-4" style="color:#FFFFFF"><b>Hello Coders</b></h1>
  <p class="lead" style="color:#FFFFFF"><b>Welcome to our store, please take a look for the product you might buy.</b></p>
</div>

<form action="hitung.php" method="post" name="listmodul">
<div class="container">
  <div class="card-deck mb-3 text-left">
    <div class="card mb-4 shadow-sm">
      <div class="card-header">
        <h4 class="my-0 font-weight-normal"><img src="img/domain.png" style="width:245px;height:200px;"></h4>
      </div>
      <div class="card-body">
        <h4 class="#"><b>Learning Basic Web Programming</b></h4>
        <p>Rp.210.000,-</p>
        <br>
        <p>Membuat anda lebih jago dalam membuat web</p>
        <button type="Submit" name="menu1" class="btn btn-lg btn-block btn-primary" data-toggle="modal" data-target="#tambah">BUY</button>
      </div>
    </div>
    <div class="card mb-4 shadow-sm">
      <div class="card-header">
        <h4 class="my-0 font-weight-normal"><img src="img/java.png" style="width:245px;height:200px;"></h4>
      </div>
      <div class="card-body">
        <h4 class="#"><b>Java Programming</b></h4>
        <br>
        <p>Rp.240.000,-</p>
        <br>
        <p>Membuat anda lebih jago dalam java language programming</p>
        <button type="Submit" name="menu2" class="btn btn-lg btn-block btn-primary" data-toggle="modal" data-target="#tambah">BUY</button>
      </div>
    </div>
    <div class="card mb-4 shadow-sm">
      <div class="card-header">
        <h4 class="my-0 font-weight-normal"><img src="img/icon.png" style="width:245px;height:200px;"></h4>
      </div>
      <div class="card-body">
        <h4 class="#"><b>Phyton Programming</b></h4>
        <br>
        <p>Rp.255.000,-</p>
        <br>
        <p>Membuat anda lebih jago dalam menangkap ular phyton wkwkw</p>
        <button type="Submit" name="menu3" class="btn btn-lg btn-block btn-primary" data-toggle="modal" data-target="#tambah">BUY</button>
      </div>
    </div>
  </div>
  </form>

  <footer class="pt-4 my-md-5 pt-md-5 border-top">
    <div class="row">
      <div class="col-12 col-md">
        <center><small class="d-block mb-3 text-muted">&copy; EAD STORE</small></center>
      </div>
      </div>
  </footer>
</div>

<div class="modal fade" id="tambah" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <div class="alert alert-success" role="alert">
  Berhasil Ditambahkan Ke Keranjang !
</div>
      </div>
      <div class="modal-body">
        <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        <input type="Submit" name="masukkan" value="OK" class="btn btn-primary" data-dismiss="modal">

      </div>
      </div>
  </div>
</div>

<?php
    if(isset($_SESSION['username'])){
        if(isset($_POST['menu1'])){
            $_SESSION['website_harga'] = 210000;
            $_SESSION['website_desc'] = "Learning basic web programming";
        }if(isset($_POST['menu2'])){
            $_SESSION['python_harga'] = 240000;
            $_SESSION['pyton_desc'] = "Starting programming in python";
        }if(isset($_POST['menu3'])){
            $_SESSION['java'] = 255000;
            $_SESSION['java_website'] = "Starting programming in java";
        }
    }
    ?>
</body>
</html>
