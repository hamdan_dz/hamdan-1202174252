<!DOCTYPE html>
<html>
<head>
	<title>EAD Store</title>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.5">

    <!-- bootstrap -->
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<!-- js -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

<?php
// Create database connection using config file
include_once("koneksi.php");

// Fetch all users data from database
$result = mysqli_query($mysqli, "SELECT * FROM users ORDER BY id DESC");
session_start();
?>

<style type="text/css">
	.batas{
		margin-top: 5%;
		margin-left: 15%;
		margin-right: 15%;
	}
</style>
</head>
<body>
<?php
  $id = $_SESSION['id'];
    $rs = mysqli_query($mysqli,"SELECT * FROM users WHERE id = '$id'");
    $data = mysqli_fetch_assoc($rs);
    $email = $data['email'];
    $username = $data['username'];
    $phone = $data['mobile_number'];
    $pass = $data['password'];
    $fullname = $data['fullname'];
?>

<div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom shadow-sm">
  <h5 class="my-0 mr-md-auto font-weight-normal"><img src="img/EAD.png" style="width:160px;height:40px;"></h5>
  <nav class="my-2 my-md-0 mr-md-3">
    
    
    
    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Hello, <?= $_SESSION['username']; ?></a>
    <div class="dropdown-menu">
      <a class="dropdown-item" href="indexfull.php">Home</a>
      <a class="dropdown-item" href="profile.php">Profile</a>
      <a class="dropdown-item" href="cart.php">Cart</a>
      <div class="dropdown-divider"></div>
      <a class="dropdown-item" href="logout.php">Logout</a>
    </div>
  </li>
  </nav>
</div>

<div class="batas">
<form action="profile.php" method="post" name="formprofile">
  <div class="form-group row">
    <label for="staticEmail" class="col-sm-2 col-form-label">Email</label>
    <div class="col-sm-10">
      <input type="text" readonly class="form-control-plaintext" name="email" value=<?php echo $email;?>>
    </div>
  </div>
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Username</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" name="username" placeholder="Username" value=<?php echo $username;?>>
    </div>
    </div>
    <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Fullname</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" name="fullname" placeholder="Username" value=<?php echo $fullname;?>>
    </div>
    </div>
    <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Mobile Number</label>
    <div class="col-sm-10">
      <input type="number" class="form-control" name="phone" placeholder="Username" value=<?php echo $phone;?>>
    </div>
    </div>
    <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">New Password</label>
    <div class="col-sm-10">
      <input type="password" class="form-control" name="password" placeholder="Password">
    </div>
    </div>
    
    <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Confirm Password</label>
    <div class="col-sm-10">
      <input type="password" class="form-control" name="password" placeholder="Retype Your Password">
    </div>
    </div>

<br>
<input type="Submit" name="save" value="Save" class="btn btn-primary btn-md btn-block">
<button type="button" class="btn btn-outline-primary btn-md btn-block"><a href="index.php">Cancel</a></button>
</div>
</form>
<footer class="pt-4 my-md-5 pt-md-5 border-top">
    <div class="row">
      <div class="col-12 col-md">
        <center><small class="d-block mb-3 text-muted">&copy; EAD STORE</small></center>
      </div>
      </div>
  </footer>

<?php
        if(isset($_POST['save'])){
            $username = $_POST['username'];
            $fullname = $_POST['fullname'];
            $number = $_POST['phone'];
            $password = $_POST['password'];
            $rs = mysqli_query($mysqli,"UPDATE users SET fullname='$fullname' , username = '$username', mobile_number = '$number', password ='$password' WHERE email = '$email'");
            if($rs){
                echo "<script>
                           alert('Profile berhasil dirubah');
                    </script>";
            }else{
                "<script>
                           alert('Something wrong...');
                    </script>";
            }
        }
    ?>

</body>
</html>