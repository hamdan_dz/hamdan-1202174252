<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.5">
	<title>EAD Store</title>

	<!-- bootstrap -->
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<!-- js -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

<?php
include_once("koneksi.php");
session_start();
$result = mysqli_query($mysqli, "SELECT * FROM users ORDER BY id DESC");
$selected = mysqli_query($mysqli, "SELECT * FROM dbcart ORDER BY id DESC");
?>

<style type="text/css">
	.batas{
	margin-top: 5%;
	margin-left: 15%;
	margin-right: 15%;
	}
</style>
</head>
<body>

<div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom shadow-sm">
  <h5 class="my-0 mr-md-auto font-weight-normal"><img src="img/EAD.png" style="width:160px;height:40px;"></h5>
  <nav class="my-2 my-md-0 mr-md-3">
    
    
    
    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Hello, <?= $_SESSION['username']; ?></a>
    <div class="dropdown-menu">
      <a class="dropdown-item" href="indexfull.php">Home</a>
      <a class="dropdown-item" href="profile.php">Profile</a>
      <a class="dropdown-item" href="cart.php">Cart</a>
      <div class="dropdown-divider"></div>
      <a class="dropdown-item" href="logout.php">Logout</a>
    </div>
  </li>
  </nav>
</div>

<div class="batas">
<table class="table table-striped">
  <thead>
    <tr>
      <th scope="col">No</th>
      <th scope="col">Product</th>
      <th scope="col">Price</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
    <?php
    foreach ($selected as $row) :
      ?>
    <tr>
      <th scope="row">1</th>
      <td><?php echo $row["product"]; ?></td>
      <td><?php echo $row["price"]; ?></td>
      <td><a href="delete.php?id=<?= $row['id'];?>"><button type="button" class="btn btn-danger"><img src="img/x-button.png" style="width: 20px;height: 20px;"></button></a></td>
      
    </tr>
    <?php endforeach; ?>
    <tr>
    	<th scope="row"></th>
    	<td colspan="1"><b>TOTAL</b></td>
    	<td><?php
      $harga=0;
    foreach ($selected as $row) :
      $harga+=$row["price"];
    endforeach;
    echo $harga;
      ?></td>
    </tr>
  </tbody>
</table>
</div>
<footer class="pt-4 my-md-5 pt-md-5 border-top">
    <div class="row">
      <div class="col-12 col-md">
        <center><small class="d-block mb-3 text-muted">&copy; EAD STORE</small></center>
      </div>
      </div>
  </footer>

  


</body>
</html>