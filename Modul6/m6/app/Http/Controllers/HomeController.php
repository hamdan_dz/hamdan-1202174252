<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\User;
use App\coment;
use Auth;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = User::all();
        $posts = Post::with('comments.user')->paginate();
        
        return view('home',compact('posts','user'));
    }

    public function detail($user_id)
    {
        $post = Post::where('id', $user_id)->get();
        $comment = coment::where('post_id', $user_id)->get();


        return view('detail', compact('post','comment'));
    }

    public function profile()
    {
        $user = User::all();
        $posts = Post::with('comments.user')->paginate();
        $prof = Auth::user();
        $pos =Post::where('user_id', Auth::user()->id )->get();

        return view('profile', compact('prof','posts','user','pos'));
    }



    public function newpost()
    {

        return view('newpost');
    }

    public function newpostaction(Request $Request)
    {
       $post= new Post();

       $post->user_id=$Request->input('user_id');
       $post->caption=$Request->input('caption');
       $post->likes=$Request->input('likes');

       $file=$Request->file('foto');
       if (!$file) {
        return redirect()->route('newpost')->with('alert','foto harus diisi!');
    }
    $file_name=$file->getClientOriginalName();
    $path=public_path('/img');
    $file->move($path,$file_name);
    $post->image='img/'.$file_name;

    $post->save();
    return redirect()->route('profile')->with('succes','Data telah diedit');
}

public function editprofile()
{

    return view('edit-profile');
}

public function editprofil(Request $request,$id)
{     

    $post = User::find($id);
    $post->title = $request->title;
    $post->description = $request->description;
    $post->url = $request->url;

    $post->avatar = $request->avatar;

    $file = $request->file('avatar');

    if (!$file) {
        return redirect()->route('profile')->with('alert','Data Harus terisi');
    }

    $file_name = $file->getClientOriginalName();
    $path = public_path("/img/");
    $file->move($path, $file_name);
    $post->avatar = $file_name;



    $post->save();


    return redirect()->route('profile')->with('succes','Data telah diedit');
}

public function addkomen(Request $request)
{
    $post = new Coment();
    $post->user_id = $request->user_id;
    $post->post_id = $request->post_id;
    $post->comment = $request->comment;
    $post->save();

    return redirect('home');
}

public function addkomen2(Request $request)
{
    $post = new Coment();
    $post->user_id = $request->user_id;
    $post->post_id = $request->post_id;
    $post->comment = $request->comment;
    $post->save();

    return redirect('home/detail/'.$post->post_id);
}

public function likes(Request $request)
{
    $gg = $request->input('post_id');
    $jml = Post::where('id', $gg)->value('likes');
    $post = Post::find($gg);
    $post->likes = $jml+1;

    $post->save();

    return redirect('home/detail/'.$gg);

}

}
