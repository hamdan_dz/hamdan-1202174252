@extends('layouts.app')

@section('content')

<div class="container" style="padding-top: 60px;">
    <div class="row">

        <div class="col-md-3">
            <center>
                <img src="../img/{{ Auth::user()->avatar }}" style="position: relative; width: 200px; height: 200px; border-radius:50%;">
            </center>
        </div>

        <div class="col-md-6" style="padding-left: 60px;">
            <h6>{{ Auth::user()->name }}</h6>

            <div style="padding-top: 20px;">
                <a href="{{route('editprofile', $prof->id)}}">Edit Profile</a>
            </div>


            <div style="padding-top: 2px;">
                <b>{{ \App\Post::where('user_id', Auth::user()->id )->count() }} </b>Post
            </div>

            
            <div style="padding-top: 20px;">
                <b>{{ Auth::user()->title }}</b>
                <p>{{ Auth::user()->description }}</p>
                <a href="{{ Auth::user()->url }}">{{ Auth::user()->url }}</a>
            </div>
            
            
        </div>

        <div class="col-md-3 text-right">
            <a href="{{route('newpost')}}">Add New Post</a>
        </div>

    </div>



    <div class="row" style="padding-top: 70px;">

        @foreach($pos as $p)
        <div class="col-md-4" style="padding-top: 30px;">
            <div class="card">
                <div class="card-body2">
                    <a href="{{route('detail', $p->id)}}">
                        <img src="../{{ $p->image }}" style="position: relative; width: 100%;">
                    </a>
                </div>
            </div>
        </div>
        @endforeach

    </div>
</div>
@endsection
