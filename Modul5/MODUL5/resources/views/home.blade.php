@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            @foreach($users as $haha)
            <div class="card">
                <div class="card-header">
                    <img src="{{ Auth::user()->avatar }}" alt="profile" class="rounded-circle" style="width: 1cm; height: 1cm;"> <b>{{ Auth::user()->name }}</b>
                </div>

                <div class="card-body">
                    <!-- @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif -->
                <center>
                <img src="{{ $haha->image }}" alt="" style="width: 75%; height: 75%; margin: 1cm;">
                </center>
                    <div class="card-header">
                        <b>{{ Auth::user()->name }}</b><br>
                        <p>{{ $haha->caption }}</p>
                </div>

            </div>
            @endforeach
        </div>

    </div>
    
</div>

</div>
@endsection


