<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('users')->insert([
            'name' => 'Hamdan Dzikru',
            'email' => 'Hamdan'.'@gmail.com',
            'password' => bcrypt('password'),
            'avatar' => 'img/foto igracias.jpg',
        ]);
        //
    }
}
